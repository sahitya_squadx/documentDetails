var mongoose = require('mongoose');

var Schema = mongoose.Schema;

mongoose.connect('mongodb://localhost:27017/documentdetails');

var userKycDetailSchema = Schema({

    user_id: { type: String },
    pan_card_submitted: { type: Boolean },
    pan_card_details: { type: mongoose.Schema.ObjectId, ref: 'PancardDetails' },
    adhaar_card_submitted: { type: Boolean },
    adhaar_card_details: { type: mongoose.Schema.ObjectId, ref: 'AdhaarcardDetails' }
});

var UserDetails = module.exports = mongoose.model('UserDetails', userKycDetailSchema);

module.exports.InsertUserDetails = function (user_details, callback) {
    console.log("User details to be inserted" + JSON.stringify(user_details));
    var user_query = { user_id: user_details.user_id };
    UserDetails.findOne(user_query, function (err, user_details_saved) {
        if (err) {
            console.log('Error Occured :: ' + err);
            throw err;
        } else {
            if (!user_details_saved) {
                console.log('User details saved ' + JSON.stringify(user_details));
                var newUserdata = UserDetails(user_details);
                newUserdata.save(callback)
            } else {
                console.log("User with this Device Id Already Exist");
                return callback(null, user_details_saved);
            }
        }
    });
};

module.exports.FindUser = function (user_id, callback) {
    var user_search_query = { user_id: user_id };
    UserDetails.findOne(user_search_query, function (err, foundUser) {

        if (err) {
            console.log('Error while Searching user :: ' + err);
            throw err;
        } else {
            if (!foundUser) {
                console.log('User with this Device Id doesnt exist');
                return callback(null, null)
            } else {
                console.log('User with thid device Id Already exist');
                return callback(null, foundUser);
            }
        }
    });
};


// User Already exist with other document but not Pancard
module.exports.UpdatePancardDetails = function (user_details, callback) {

    var user_id_query = { user_id: user_details.user_id }
    var pancard_detail_objectId = user_details.pancard_id;
    UserDetails.findOneAndUpdate(user_id_query,
        {
            $set: { pan_card_submitted: true, pan_card_details: pancard_detail_objectId }
        }, { new: true }, function (err, updated_user) {

            if (err) {
                console.log('Error while Inserting Pancard in usercollection :: ' + err);
                throw err;
            } else {
                console.log('User id updated Successfully');
                return callback(null, updated_user);
            }
        });
};


//User Already exist with other Document but not with Adhaarcard
module.exports.UpdateAdhaarcardDetails = function (adhaar_card_details, callback) {

    var user_id_query = { user_id: adhaar_card_details.user_id };
    var adhaarcard_details = adhaar_card_details.adhaar_card_details;

    UserDetails.findOneAndUpdate(user_id_query, {
        $set: { adhaar_card_submitted: true, adhaar_card_details: adhaarcard_details }
    }, { new: true }, function (err, upadated_adhaarcard) {

        if (err) {
            console.log('Error while updating user Adhaar to server ' + err);
            throw err;
        } else {
            console.log('User info and Adhaar card updated Successfully');
            return callback(null, upadated_adhaarcard);
        }
    });
}

module.exports.InserUserPancardDetails = function (user_pancard_details, callback) {

    console.log('User Details with Pancard to be entered' + JSON.stringify(user_pancard_details));

    var user_id = { user_id: user_pancard_details.user_id };

    UserDetails.findOne(user_id, function (err, user_details_found) {
        if (err) {
            console.log('Error while searching User :: ' + err);
            throw err;
        } else {
            if (user_details_found) {
                console.log('Saved user found :: ' + user_details_found);
                return callback(user_details_found);
            } else {
                console.log('User not Found');
                return callback(null);
            }
        }
    });

};

module.exports.InsertAdhaarCardDetails = function (user_adhaarcard_details, callback) {
    console.log('User new Adhaarcard Details to be Inersted ' + user_adhaarcard_details);

    var user_id = { user_id: user_adhaarcard_details.user_id };

    UserDetails.findOne(user_id, function (err, user_details_found) {
        if (err) {
            console.log('Error while inserting users Adhaar data ' + err);
            throw err;
        } else {

            if (user_adhaarcard_details) {

                return callback(user_adhaarcard_details);
            } else {
                return callback(null);
            }
        }
    });

}