var mongoose = require('mongoose');

var Schema = mongoose.Schema;

mongoose.connect('mongodb://localhost:27017/documentdetails');

var adhaarcardSchema = new Schema({


    adhaar_number: { type: String, required: true },
    user_name: { type: String },
    user_address: { type: String },
    user_city: { type: String },
    user_state: { type: String },
    user_pincode: { type: String },
    user_gender: { type: String },
    user_district: { type: String },
    user_village_town: { type: String },
    user_dob: { type: String }
});


var AdhaarcardDetails = module.exports = mongoose.model('AdhaarcardDetails', adhaarcardSchema);

module.exports.InsertAdhaarCardDetails = function (user_adhaarcard_details, callback) {

    console.log("User Adhaarcard details to be Inserted :: " + JSON.stringify(user_adhaarcard_details));


    var user_adhaarnumber = user_adhaarcard_details.adhaar_number;

    var query_search_adhaar_number = { adhaar_number: user_adhaarnumber };

    AdhaarcardDetails.findOne(query_search_adhaar_number, function (err, adhaar_details) {
        if (err) {
            console.error("Error while inserting Adhaar details :: " + err);
            throw err;
        } else {
            if (!adhaar_details) {
                console.log("Inserting new Adhaar details......");
                var inserted_data = new AdhaarcardDetails(user_adhaarcard_details);
                inserted_data.save(callback);
            } else {
                console.error("User details with this Adhaar number is already Exist");
                return callback(null, adhaar_details);
            }
        }

    });

};

module.exports.FindAdhaarcard = function (adhaar_number_id, callback) {

    var query_search_adhaar_number = { adhaar_number: adhaar_number_id };

    AdhaarcardDetails.findOne(query_search_adhaar_number, function (err, found_user) {
        if (err) {
            console.log('Found error while searching adhaar number ' + err);
            throw err;
        } else {
            if (found_user) {
                return callback(null, found_user, true);
            } else {
                return callback(null, null, false);
            }
        }
    });
};

module.exports.FindAdhaarNumberById = function (adhaar_object_number, callback) {
    AdhaarcardDetails.findById(adhaar_object_number, function (err, found_user) {
        if (err) {
            console.log('Found error while searching adhaar number ' + err);
            throw err;
        } else {
            if (found_user) {
                return callback(null, found_user, true);
            } else {
                return callback(null, null, false);
            }
        }
    });
}
