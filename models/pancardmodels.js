var mongoose = require('mongoose');

var Schema = mongoose.Schema;

mongoose.connect('mongodb://localhost:27017/documentdetails');

var pancardSchema = new Schema({

    pan_number: { type: String, required: true },
    pan_user_name: { type: String, required: true },
    pan_users_father_name: { type: String, required: true },
    pan_user_gender: { type: String, required: true },
    pan_user_dob: { type: String, required: true },
    user_pan_card_image: { type: String }

});

var PancardDetails = module.exports = mongoose.model('PancardDetails', pancardSchema);


module.exports.InsertUserPancard = function (pan_details, callback) {
    console.log("User details that to be inserted is :: " + JSON.stringify(pan_details));
    var pan_number = pan_details.pan_number;
    var query_to_serach_pan_number = { pan_number: pan_number };

    PancardDetails.findOne(query_to_serach_pan_number, function (err, add_pandetails) {
        if (err) {
            console.error("Error while inserting Pan Details ::: --> " + err);
            return callback(err, null);
        } else {
            if (!add_pandetails) {
                console.log("Inserting new Pancard details......");
                var inserted_data = new PancardDetails(pan_details);
                inserted_data.save(callback);
            } else {
                console.log("Pancard with this number is already exist");
                return callback(null, add_pandetails);
            }
        }
    });
};


module.exports.FindPancardNumber = function (pan_number, callback) {

    var find_user_pan = { pan_number: pan_number };

    PancardDetails.findOne(find_user_pan, function (err, found_pandetails) {
        if (err) {
            console.log('Found error while checking user data ' + err);
            throw err;
        } else {
            if (found_pandetails) {
                return callback(null, found_pandetails, true);
            } else {
                return callback(null, null, false);
            }
        }
    });
};

module.exports.FindPancardNumberById = function (pan_number_id, callback) {
    PancardDetails.findById(pan_number_id, function (err, found_pandetails) {
        if (err) {
            console.log('Erro while Searching userpancar with PanId ;; ' + err);
            throw err;
        } else {
            if (found_pandetails) {
                return callback(null, found_pandetails, true);
            } else {
                return callback(null, null, false);
            }
        }
    });
};

