var express = require('express');
var fs = require('fs');
var pancardDetail = require('../models/pancardmodels');
var router = express.Router();
var Config = require('../Config');


router.post('/add_pancard_details', function (req, res, next) {

    // console.log("Response Parser " + JSON.stringify(req.body));


    var image = "data:image/jpg;base64," + req.body.user_pan_card_image;
    var data = image.replace(/^data:image\/\w+;base64,/, "");
    var buf = new Buffer(data, 'base64');
    var image_name = req.body.pan_number + "_image.jpg";
    fs.writeFile('./user_pancards/' + image_name, buf, function (err, data) {

        if (err) {
            console.error("Error while writting image to the DB " + err);
        } else {

            var image_path = Config.server_url +"user-pancard/"+ image_name;

            console.log("Image name is :: " + image_name + "Image path :: ", image_path);


            var user_data_insert = {
                pan_number: req.body.pan_number,
                pan_user_name: req.body.pan_user_name,
                pan_users_father_name: req.body.pan_users_father_name,
                pan_user_gender: req.body.pan_user_gender,
                pan_user_dob: req.body.pan_user_dob,
                user_pan_card_image: image_path
            };


            pancardDetail.InsertUserPancard(user_data_insert, function (err, pancard, alreadyexisting) {

                if (err) {
                    console.error("Error while inserting " + err);
                    var error_response = {
                        status: 0,
                        message: 'Failed to insert details',
                        reason: 'Reason to failed ' + err
                    }
                    res.send(JSON.stringify(error_response));
                } else {

                    if (alreadyexisting) {
                        var inserted_data = {
                            status: 0,
                            message: 'User Data with this pancard already exist.',
                        }
                        console.log("Pan Details inserted sucessfully" + pancard);
                        res.send(JSON.stringify(inserted_data));

                    } else {
                        var inserted_data = {
                            status: 1,
                            message: 'User Detail Inserted Succesfuly',
                            user_detail: pancard
                        }
                        console.log("Pan Details inserted sucessfully" + pancard);
                        res.send(JSON.stringify(inserted_data));
                    }
                }
            });


        }

    });


});

module.exports = router;
