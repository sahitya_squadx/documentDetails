var express = require('express');
var fs = require('fs');
var UserDetails = require('../models/userkymodels');
var UserPancard = require('../models/pancardmodels');
var UserAdhaarcard = require('../models/adhaarcardmodels');
var router = express.Router();
var Config = require('../Config');

router.post('/userpancard', function (req, res, next) {
    var device_id = req.body.user_id;
    UserDetails.FindUser(device_id, function (err, found_user) {
        if (err) {
            console.log('Error found while searching User ' + err);
            throw err;
        } else {

            if (found_user) {
                var user_id = found_user.user_id;
                var pan_card_submitted = found_user.pan_card_submitted;
                if (pan_card_submitted) {
                    var response_for_already_submitted = {
                        status: '0',
                        message: 'This user had already Submitted its Pancard'
                    }
                    res.send(response_for_already_submitted);
                } else {

                    var is_adhaarcard_submitted = found_user.adhaar_card_submitted;

                    if (is_adhaarcard_submitted) {


                        var adhaar_card_id = found_user.adhaar_card_details;

                        UserAdhaarcard.FindAdhaarNumberById(adhaar_card_id, function (err, found_user_adhaar) {

                            if (err) {
                                console.log('Error while Searching Adhaar card' + err);
                                throw err;
                            } else {
                                var adhaar_user_name = found_user_adhaar.user_name;
                                var adhaar_user_dob = found_user.user_dob;
                                if (adhaar_user_name === req.body.pan_user_name || adhaar_user_dob === req.body.pan_user_dob) {
                                    UserPancard.FindPancardNumber(req.body.pan_number, function (err, found_user, is_pancard_exist) {

                                        if (is_pancard_exist) {
                                            var pancard_already_exist = {
                                                status: "0",
                                                message: "This pancard belongs to other user"
                                            }
                                            res.send(pancard_already_exist);
                                        } else {

                                            var image = "data:image/jpg;base64," + req.body.user_pan_card_image;
                                            var data = image.replace(/^data:image\/\w+;base64,/, "");
                                            var buf = new Buffer(data, 'base64');
                                            var image_name = req.body.pan_number + "_image.jpg";

                                            fs.writeFile('./user_pancards/' + image_name, buf, function (err, data) {

                                                if (err) {
                                                    console.error("Error while writting image to the DB " + err);
                                                } else {

                                                    var image_path = Config.server_url + "user-pancard/" + image_name;

                                                    console.log("Image name is :: " + image_name + "Image path :: ", image_path);


                                                    var user_data_insert = {
                                                        pan_number: req.body.pan_number,
                                                        pan_user_name: req.body.pan_user_name,
                                                        pan_users_father_name: req.body.pan_users_father_name,
                                                        pan_user_gender: req.body.pan_user_gender,
                                                        pan_user_dob: req.body.pan_user_dob,
                                                        user_pan_card_image: image_path
                                                    };


                                                    UserPancard.InsertUserPancard(user_data_insert, function (err, pancard) {

                                                        if (err) {
                                                            console.error("Error while inserting " + err);
                                                            var error_response = {
                                                                status: 0,
                                                                message: 'Failed to insert details',
                                                                reason: 'Reason to failed ' + err
                                                            }
                                                            res.send(JSON.stringify(error_response));
                                                        } else {

                                                            if (pancard) {

                                                                var user_pancard_id = pancard._id;

                                                                var updating_data = {
                                                                    user_id: found_user.user_id,
                                                                    pancard_id: user_pancard_id
                                                                }

                                                                UserDetails.UpdatePancardDetails(updating_data, function (err, updated_user_data) {

                                                                    if (err) {
                                                                        console.log('Error while Updating user data ' + err);
                                                                        throw err;
                                                                    } else {

                                                                        console.log('New Value to be updated ' + updated_user_data)


                                                                        var success_update = {
                                                                            status: 1,
                                                                            message: 'User Pancard Details updated Successfully',
                                                                            user_details: updated_user_data
                                                                        }

                                                                        res.send(success_update);

                                                                    }


                                                                });
                                                            }



                                                        }
                                                    });


                                                }

                                            });

                                        }
                                    });

                                } else {

                                    var other_user_doc = {
                                        status: "0",
                                        message: "Your Pancard didnt matched with Adhaar Document"

                                    }
                                    res.send(other_user_doc);


                                }

                            }


                        });



                    } else {

                        UserPancard.FindPancardNumber(req.body.pan_number, function (err, found_user, is_pancard_exist) {

                            if (is_pancard_exist) {
                                var pancard_already_exist = {
                                    status: "0",
                                    message: "This pancard belongs to other user"
                                }
                                res.send(pancard_already_exist);
                            } else {

                                var image = "data:image/jpg;base64," + req.body.user_pan_card_image;
                                var data = image.replace(/^data:image\/\w+;base64,/, "");
                                var buf = new Buffer(data, 'base64');
                                var image_name = req.body.pan_number + "_image.jpg";

                                fs.writeFile('./user_pancards/' + image_name, buf, function (err, data) {

                                    if (err) {
                                        console.error("Error while writting image to the DB " + err);
                                    } else {

                                        var image_path = Config.server_url + "user-pancard/" + image_name;

                                        console.log("Image name is :: " + image_name + "Image path :: ", image_path);


                                        var user_data_insert = {
                                            pan_number: req.body.pan_number,
                                            pan_user_name: req.body.pan_user_name,
                                            pan_users_father_name: req.body.pan_users_father_name,
                                            pan_user_gender: req.body.pan_user_gender,
                                            pan_user_dob: req.body.pan_user_dob,
                                            user_pan_card_image: image_path
                                        };


                                        UserPancard.InsertUserPancard(user_data_insert, function (err, pancard) {

                                            if (err) {
                                                console.error("Error while inserting " + err);
                                                var error_response = {
                                                    status: 0,
                                                    message: 'Failed to insert details',
                                                    reason: 'Reason to failed ' + err
                                                }
                                                res.send(JSON.stringify(error_response));
                                            } else {

                                                if (pancard) {

                                                    var user_pancard_id = pancard._id;

                                                    var updating_data = {
                                                        user_id: found_user.user_id,
                                                        pancard_id: user_pancard_id
                                                    }

                                                    UserDetails.UpdatePancardDetails(updating_data, function (err, updated_user_data) {

                                                        if (err) {
                                                            console.log('Error while Updating user data ' + err);
                                                            throw err;
                                                        } else {

                                                            console.log('New Value to be updated ' + updated_user_data)


                                                            var success_update = {
                                                                status: 1,
                                                                message: 'User Pancard Details updated Successfully',
                                                                user_details: updated_user_data
                                                            }

                                                            res.send(success_update);

                                                        }


                                                    });
                                                }



                                            }
                                        });


                                    }

                                });

                            }
                        });
                    }

                }

            } else {

                UserPancard.FindPancardNumber(req.body.pan_number, function (err, found_user, is_pancard_exist) {

                    if (is_pancard_exist) {
                        var pancard_already_exist = {
                            status: "0",
                            message: "This pancard belongs to other user"
                        }
                        res.send(pancard_already_exist);
                    } else {


                        var image = "data:image/jpg;base64," + req.body.user_pan_card_image;
                        var data = image.replace(/^data:image\/\w+;base64,/, "");
                        var buf = new Buffer(data, 'base64');
                        var image_name = req.body.pan_number + "_image.jpg";

                        fs.writeFile('./user_pancards/' + image_name, buf, function (err, data) {

                            if (err) {
                                console.error("Error while writting image to the DB " + err);
                            } else {

                                var image_path = Config.server_url + "user-pancard/" + image_name;

                                console.log("Image name is :: " + image_name + "Image path :: ", image_path);


                                var user_data_insert = {
                                    pan_number: req.body.pan_number,
                                    pan_user_name: req.body.pan_user_name,
                                    pan_users_father_name: req.body.pan_users_father_name,
                                    pan_user_gender: req.body.pan_user_gender,
                                    pan_user_dob: req.body.pan_user_dob,
                                    user_pan_card_image: image_path
                                };


                                UserPancard.InsertUserPancard(user_data_insert, function (err, pancard) {

                                    if (err) {
                                        console.error("Error while inserting " + err);
                                        var error_response = {
                                            status: 0,
                                            message: 'Failed to insert details',
                                            reason: 'Reason to failed ' + err
                                        }
                                        res.send(JSON.stringify(error_response));
                                    } else {


                                        if (pancard) {

                                            console.log("new Pancard Details " + JSON.stringify(pancard) + " id " + pancard._id);
                                            var user_pancard_id = pancard._id;


                                            var insert_new_user_pancard = {
                                                user_id: device_id,
                                                pan_card_submitted: true,
                                                pan_card_details: user_pancard_id
                                            }

                                            var updated_userdetails = new UserDetails(insert_new_user_pancard);
                                            updated_userdetails.save(function (err, saved_user) {

                                                if (err) {
                                                    console.log('Error Found while saving new data of User ' + err);
                                                    throw err;
                                                } else {

                                                    var send_response = {
                                                        status: 1,
                                                        message: 'New user Data Submitted Successfully',
                                                        user_saved: saved_user
                                                    }

                                                    res.send(send_response);
                                                }
                                            });

                                        }


                                    }
                                });
                            }

                        });
                    }

                });




            }

        }

    });

});



router.post('/adhaarcard', function (req, res, next) {


    var device_id = req.body.user_id;

    UserDetails.FindUser(device_id, function (err, found_user) {
        if (err) {
            console.error('Error while Seraching User Id' + err);
            throw err;
        } else {
            console.log('User to be inserted =-=-=-==-= ' + JSON.stringify(found_user));

            if (found_user) {

                var is_adhaar_submitted = found_user.adhaar_card_submitted;
                var is_pancard_submitted = found_user.pan_card_submitted;

                if (is_adhaar_submitted) {
                    console.log('User Adhaar card is already Submitted');
                    var res_json = {
                        status: '0',
                        message: 'Adhaar card is Already Submitted with this user',
                        user_details: found_user
                    }
                    res.send(res_json);
                } else {

                    if (is_pancard_submitted) {
                        console.log("Found User is :::+++ " + JSON.stringify(found_user));
                        var pancard_id = found_user.pan_card_details;

                        console.log("user pancard id " + pancard_id);
                        UserPancard.FindPancardNumberById(pancard_id, function (err, found_pancard) {

                            if (err) {
                                console.log('Error fond while inserting data ' + err);
                                throw err;
                            } else {

                                var user_name = found_pancard.pan_user_name;
                                var user_dob = found_pancard.pan_user_dob

                                if (user_name === req.body.user_name || user_dob === req.body.user_dob) {
                                    UserAdhaarcard.FindAdhaarcard(req.body.adhaar_number, function (err, found_adhar, is_exist) {

                                        if (err) {
                                            console.log('Error :: ' + err);
                                            throw err;
                                        } else {
                                            if (is_exist) {
                                                var adhaar_already_exist = {
                                                    status: "0",
                                                    message: "This Adhaar belongs to other user"
                                                }
                                                res.send(adhaar_already_exist);

                                            } else {
                                                var user_data_insert = {
                                                    adhaar_number: req.body.adhaar_number,
                                                    user_name: req.body.user_name,
                                                    user_address: req.body.user_address,
                                                    user_city: req.body.user_city,
                                                    user_state: req.body.user_state,
                                                    user_pincode: req.body.user_pincode,
                                                    user_gender: req.body.user_gender,
                                                    user_district: req.body.user_district,
                                                    user_village_town: req.body.user_village_town,
                                                    user_dob: req.body.user_dob
                                                };

                                                UserAdhaarcard.InsertAdhaarCardDetails(user_data_insert, function (err, new_inserted_users_adhaar) {


                                                    if (err) {

                                                        console.log('Error while inserting User Adhaar card');
                                                        throw err;

                                                    } else {
                                                        if (new_inserted_users_adhaar) {

                                                            console.log("Adhaar Card Details" + JSON.stringify(new_inserted_users_adhaar));

                                                            var user_adhaarcard_id = new_inserted_users_adhaar._id;
                                                            console.log("");
                                                            var updating_data = {
                                                                user_id: found_user.user_id,
                                                                adhaar_card_details: user_adhaarcard_id
                                                            }

                                                            UserDetails.UpdateAdhaarcardDetails(updating_data, function (err, updated_user_data) {

                                                                if (err) {
                                                                    console.log('Error while Updating user data ' + err);
                                                                    throw err;
                                                                } else {

                                                                    console.log('New Value to be updated ' + updated_user_data)

                                                                    var success_update = {
                                                                        status: 1,
                                                                        message: 'User Pancard Details updated Successfully',
                                                                        user_details: updated_user_data
                                                                    }

                                                                    res.send(success_update);

                                                                }


                                                            });

                                                        }
                                                    }

                                                });

                                            }

                                        }


                                    });


                                } else {

                                    var other_user_doc = {
                                        status: "0",
                                        message: "Your Adhaar Document didnt matched with Pancard"

                                    }
                                    res.send(other_user_doc);

                                }

                            }



                        });



                    } else {


                        UserAdhaarcard.FindAdhaarcard(req.body.adhaar_number, function (err, found_adhar, is_exist) {

                            if (err) {
                                console.log('Error :: ' + err);
                                throw err;
                            } else {

                                if (is_exist) {


                                    var adhaar_already_exist = {
                                        status: "0",
                                        message: "This Adhaar belongs to other user"
                                    }
                                    res.send(adhaar_already_exist);

                                } else {




                                    var user_data_insert = {
                                        adhaar_number: req.body.adhaar_number,
                                        user_name: req.body.user_name,
                                        user_address: req.body.user_address,
                                        user_city: req.body.user_city,
                                        user_state: req.body.user_state,
                                        user_pincode: req.body.user_pincode,
                                        user_gender: req.body.user_gender,
                                        user_district: req.body.user_district,
                                        user_village_town: req.body.user_village_town,
                                        user_dob: req.body.user_dob
                                    };

                                    UserAdhaarcard.InsertAdhaarCardDetails(user_data_insert, function (err, new_inserted_users_adhaar) {


                                        if (err) {

                                            console.log('Error while inserting User Adhaar card');
                                            throw err;

                                        } else {
                                            if (new_inserted_users_adhaar) {

                                                console.log("Adhaar Card Details" + JSON.stringify(new_inserted_users_adhaar));

                                                var user_adhaarcard_id = new_inserted_users_adhaar._id;
                                                console.log("");
                                                var updating_data = {
                                                    user_id: found_user.user_id,
                                                    adhaar_card_details: user_adhaarcard_id
                                                }

                                                UserDetails.UpdateAdhaarcardDetails(updating_data, function (err, updated_user_data) {

                                                    if (err) {
                                                        console.log('Error while Updating user data ' + err);
                                                        throw err;
                                                    } else {

                                                        console.log('New Value to be updated ' + updated_user_data)

                                                        var success_update = {
                                                            status: 1,
                                                            message: 'User Pancard Details updated Successfully',
                                                            user_details: updated_user_data
                                                        }

                                                        res.send(success_update);

                                                    }


                                                });

                                            }
                                        }

                                    });

                                }

                            }


                        });

                    }








                }


            } else {

                UserAdhaarcard.FindAdhaarcard(req.body.adhaar_number, function (err, found_adhar, is_exist) {

                    if (err) {
                        console.log('Error :: ' + err);
                        throw err;
                    } else {

                        if (is_exist) {

                            var adhaar_already_exist = {
                                status: "0",
                                message: "This Adhaar belongs to other user"
                            }
                            res.send(adhaar_already_exist);

                        } else {

                            var user_data_insert = {
                                adhaar_number: req.body.adhaar_number,
                                user_name: req.body.user_name,
                                user_address: req.body.user_address,
                                user_city: req.body.user_city,
                                user_state: req.body.user_state,
                                user_pincode: req.body.user_pincode,
                                user_gender: req.body.user_gender,
                                user_district: req.body.user_district,
                                user_village_town: req.body.user_village_town,
                                user_dob: req.body.user_dob
                            };

                            UserAdhaarcard.InsertAdhaarCardDetails(user_data_insert, function (err, new_inserted_users_adhaar) {


                                if (err) {
                                    console.log('Error while Inserting new user ' + err);
                                    throw err;
                                } else {

                                    if (new_inserted_users_adhaar) {


                                        var user_adhaarcard_id = new_inserted_users_adhaar._id;

                                        var updating_data = {
                                            user_id: device_id,
                                            adhaar_card_details: user_adhaarcard_id,
                                            adhaar_card_submitted: true

                                        }

                                        var updated_userdetails = new UserDetails(updating_data);
                                        updated_userdetails.save(function (err, saved_user) {

                                            if (err) {
                                                console.log('Error Found while saving new data of User ' + err);
                                                throw err;
                                            } else {

                                                var send_response = {
                                                    status: 1,
                                                    message: 'New Adhaarcard Submitted Successfully',
                                                    user_saved: saved_user
                                                }
                                                res.send(send_response);
                                            }
                                        });


                                    }



                                }





                            });

                        }

                    }


                });




            }

        }



    });


});


module.exports = router;