var express = require('express');
var adhaarcardDetails = require('../models/adhaarcardmodels');
var router = express.Router();


router.post('/adhaarcard_details', function (req, res, next) {

    console.log("Adhaar details to be inserted " + JSON.stringify(req.body));

    var user_data_insert = {
        adhaar_number: req.body.adhaar_number,
        user_name: req.body.user_name,
        user_address: req.body.user_address,
        user_city: req.body.user_city,
        user_state: req.body.user_state,
        user_pincode: req.body.user_pincode,
        user_gender: req.body.user_gender,
        user_district: req.body.user_district,
        user_village_town: req.body.user_village_town,
        user_dob: req.body.user_dob
    };

    adhaarcardDetails.InsertAdhaarCardDetails(user_data_insert, function (err, adhaarcard, isExistOrNot) {
        if (err) {

            console.error("Error while inserting " + err + " and Data is --> ", user_data_insert);
            var error_response = {
                status: 0,
                message: 'Failed to insert details',
                reason: 'Reason to failed ' + err
            }
            res.send(JSON.stringify(error_response));
        } else {

            if (isExistOrNot) {

                var inserted_data = {
                    status: 0,
                    message: 'User Data with this pancard already exist.',
                }
                console.log("Pan Details inserted sucessfully" + adhaarcard);

                res.send(JSON.stringify(inserted_data));
            } else {
                var inserted_data = {
                    status: 1,
                    message: 'User Detail Inserted Succesfuly',
                    user_detail: adhaarcard
                }
                console.log("Pan Details inserted sucessfully" + adhaarcard);
                res.send(JSON.stringify(inserted_data));
            }
        }
    });
});

module.exports = router;