var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var config = require('./Config');
var mongoose = require('mongoose');

var bodyParser = require('body-parser');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var pancardRouter = require('./routes/pancard');
var adhaarcardRouter = require('./routes/adhaarcard');
var userDetailRouter = require('./routes/useradd');


mongoose.Promise = global.Promise;
mongoose.connect(config.data_base_url);


var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');


app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/user-pancard', express.static(path.join(__dirname, 'user_pancards')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/pancard', pancardRouter);
app.use('/adhaarcard', adhaarcardRouter);
app.use('/userAdd', userDetailRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;

// app.listen(3000, function () {
//     console.log('App is Running on port no 3000');
// });
